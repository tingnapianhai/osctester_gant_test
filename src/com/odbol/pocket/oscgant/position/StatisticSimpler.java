package com.odbol.pocket.oscgant.position;

import java.util.ArrayList;
import java.util.List;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

public class StatisticSimpler{
	
private static final String TAG = "Statistic";
	
	public static double[] phone_angle = new double[3];// to show the phone's angle
	
	private static String Pietro_phone = "AndroidAP";
	public static boolean detect_Pietro_phone = false;
	public static boolean Pietro_phone_INOUT = false;
	
	public static int strongestAP = 0;
	public static int strongestAP_tplink = 0;
	public static int strongestAP_jointRouter = 0;
	public static String strongestAPname = " ";
	public static int average_logitec1 = 0;
	public static int average_logitec2 = 0;
	public static int average_logitec3 = 0;
	public static int average_tplink1 = 0;
	public static int average_tplink2 = 0;
	public static int average_pietro_phone = 0;
	
	private static final String Logitec1 = "00:01:8e:99:0d:00"; //logitec: 1
	private static final String Logitec2 = "00:01:8e:99:8d:e2"; //logitec: 2
	private static final String Logitec3 = "00:01:8e:99:8e:5c"; //logitec: 3
	private static final String TPLink1 = "a0:f3:c1:9a:47:1c"; //TP-LINK: 1***
	private static final String TPLink2 = "90:f6:52:71:8b:5b"; //TP-LINK: 2***
	public static int Logitec1_level = -100;
	public static int Logitec2_level = -100;
	public static int Logitec3_level = -100;
	public static int TPLink1_level = -100;
	public static int TPLink2_level = -100;
	public static int Pietro_level = -100;
	public static ArrayList<Integer> filter_logitec1 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_logitec2 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_logitec3 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_tplink1 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_tplink2 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_pietro_phone = initializeArrayList2(-100,-100,-100,-100,-100);
	//public static int counterINreceiver = 0;
	
	public static int counter_statistic_auto = 0;
	public static int point_number_auto = 0; //kind of useless
	public static int point_number_auto_logitec = 0;
	public static int point_number_auto_tplink = 0;
	public static int point_number_auto_jointRouter = 0;
	
	public static ArrayList<Integer> initializeArrayList2 (int AP1, int AP2, int AP3, int AP4, int AP5)
	{
		ArrayList<Integer> alist = new ArrayList<Integer> ();
		alist.add(0, AP1);
		alist.add(1, AP2);
		alist.add(2, AP3);
		alist.add(3, AP4);
		alist.add(4, AP5);
		return alist;
	}

	public static void getIndoorPositionReferenceAuto_3Logitec (WifiManager wifi) {
		
		boolean detect_logitec1 = false; //to detect if "logitec1 AP" can be detected by phone, which means if the "logitec1 AP" is visible or not
		boolean detect_logitec2 = false;
		boolean detect_logitec3 = false;
		boolean detect_AP = false; // to detect Pietro_phone
		boolean detect_tplink1 = false;
		boolean detect_tplink2 = false;
		
		wifi.startScan();
		
		List<ScanResult> mWifiList = wifi.getScanResults();
		ScanResult sr;
		int i = 0;
		if(mWifiList!=null) {
			int size = mWifiList.size();
			for(i=0; i < size; i++) {
				sr = mWifiList.get(i);
				if(sr.BSSID.equalsIgnoreCase(Logitec1)) {
					Logitec1_level = sr.level;
					detect_logitec1 = ArrayListConverting (filter_logitec1, Logitec1_level);
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec2)) {
					Logitec2_level = sr.level;
					detect_logitec2 = ArrayListConverting (filter_logitec2, Logitec2_level);
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec3)) {
					Logitec3_level = sr.level;
					detect_logitec3 = ArrayListConverting (filter_logitec3, Logitec3_level);
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink1)) {
					TPLink1_level = sr.level;
					detect_tplink1 = ArrayListConverting (filter_tplink1, TPLink1_level);
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink2)) {
					TPLink2_level = sr.level;
					detect_tplink2 = ArrayListConverting (filter_tplink2, TPLink2_level);
				}
				
				// detect Pietro_phone AndroidAP;
				else if(sr.SSID.equalsIgnoreCase(Pietro_phone)) {
					Pietro_level = sr.level;
					detect_AP = ArrayListConverting (filter_pietro_phone, Pietro_level);
				}
			} // end for
		} //end if
		
		detect_Pietro_phone = detect_AP; // to show the Pietro_phone status
		
		Logitec1_level = ArrayListConvert(filter_logitec1, detect_logitec1, Logitec1_level);
		Logitec2_level = ArrayListConvert(filter_logitec2, detect_logitec2, Logitec2_level);
		Logitec3_level = ArrayListConvert(filter_logitec3, detect_logitec3, Logitec3_level);
		TPLink1_level = ArrayListConvert(filter_tplink1, detect_tplink1, TPLink1_level);
		TPLink2_level = ArrayListConvert(filter_tplink2, detect_tplink2, TPLink2_level);
		Pietro_level = ArrayListConvert(filter_pietro_phone, detect_AP, Pietro_level);
		
	}
	
	//convert ArrayList
	public static boolean ArrayListConverting (ArrayList<Integer> alist, int instant)
	{
		alist.set(4, alist.get(3));
		alist.set(3, alist.get(2));
		alist.set(2, alist.get(1));
		alist.set(1, alist.get(0));
		alist.set(0, instant);
		return true;
		}
	
	//convert ArrayList
	public static int ArrayListConvert (ArrayList<Integer> alist, boolean detect, int instant)
	{
		if(!detect) {
			alist.set(4, alist.get(3));
			alist.set(3, alist.get(2));
			alist.set(2, alist.get(1));
			alist.set(1, alist.get(0));
			alist.set(0, -100);
			instant = -100;
			}
		return instant;
	}
	
	// hard in, harder out
	public static String getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi) {
		
		getIndoorPositionReferenceAuto_3Logitec (wifi);
		
		Monitor.Monitor_filterINnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		
		average_logitec1 = AverageSignalStrength(filter_logitec1, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec2 = AverageSignalStrength(filter_logitec2, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec3 = AverageSignalStrength(filter_logitec3, Monitor.Monitor_filterINOUTreferenceNumber);
		average_pietro_phone = AverageSignalStrength(filter_pietro_phone, Monitor.Monitor_filterINOUTreferenceNumber);
		strongestAP = ComparisonSignalStrength(average_logitec1,average_logitec2,average_logitec3,average_pietro_phone);
		
		// remember to add "return" statement ----------------------------------------------------
		int test = testStrongestFirst();
		if(test>0)
			return " " + test;
		
		switch (point_number_auto_logitec) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 0:
			if(Monitor.Monitor_filterINnumber_logitec1 >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			else if(Monitor.Monitor_filterINnumber_logitec2>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			else if(Monitor.Monitor_filterINnumber_logitec3>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(Monitor.Monitor_filterINnumber_pietro_phone >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			break;
		// in Point 1
		case 1:
			if(Monitor.Monitor_filterINnumber_logitec2>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			else if(Monitor.Monitor_filterINnumber_logitec3>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(Monitor.Monitor_filterINnumber_pietro_phone>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			//else if(NumberByFilter(filter_logitec1, filterOut)>0 )
			else if(Monitor.Monitor_filterOUTnumber_logitec1 >0 )
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			break;
		// in Point 2
		case 2:
			if(Monitor.Monitor_filterINnumber_logitec1>= Monitor.Monitor_filterINOUTreferenceNumber)
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			else if(Monitor.Monitor_filterINnumber_logitec3>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(Monitor.Monitor_filterINnumber_pietro_phone >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			
			else if(Monitor.Monitor_filterOUTnumber_logitec2>0 )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			break;
		// in Point 3
		case 3:
				if(Monitor.Monitor_filterINnumber_logitec1>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 1;
					return "logitec 1";
				}
				else if(Monitor.Monitor_filterINnumber_logitec2>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 2;
					return "logitec 2";
				}
				else if(Monitor.Monitor_filterINnumber_pietro_phone >= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 4;
					return "AndroidAP";
				}
				
				else if(Monitor.Monitor_filterOUTnumber_logitec3>0 )
				{
					point_number_auto_logitec = 3;
					return "logitec 3";
				}
				break;
		// in AndroidAP
		case 4:
				if(Monitor.Monitor_filterINnumber_logitec1>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 1;
					return "logitec 1";
				}
				else if(Monitor.Monitor_filterINnumber_logitec2 >= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 2;
					return "logitec 2";
				}
				else if(Monitor.Monitor_filterINnumber_logitec3>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 3;
					return "logitec 3";
				}
				
				else if(Monitor.Monitor_filterOUTnumber_pietro_phone>0 )
				{
					point_number_auto_logitec = 4;
					return "AndroidAP";
				}
				break;
			
		default: break;
		}// end switch
		
		point_number_auto_logitec = 0;
		
		return "no ";
	} // end getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi)
	
	private static int testStrongestFirst () {
		switch (strongestAP) {
		case 1:
			if(Monitor.Monitor_filterINnumber_logitec1 >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 1;
				return 1;
			}
		case 2:
			if(Monitor.Monitor_filterINnumber_logitec2 >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return 2;
			}
		case 3:
			if(Monitor.Monitor_filterINnumber_logitec3 >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return 3;
			}
		case 4:
			if(Monitor.Monitor_filterINnumber_pietro_phone >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return 4;
			}
		default: return 0;
		}
	}
	
	// to calculate the average-signal-strength for specific Access-Point
	private static int AverageSignalStrength (ArrayList<Integer> alist, int referenceNumber) {
		int sum = 0;
		int ave = 0;
		for(int i=0;i<referenceNumber;i++){
			sum += alist.get(i);
		}
		ave = sum/referenceNumber;
		return ave;
	}
	
	// to compare average signal-strength
	private static int ComparisonSignalStrength (int ave_logitec1, int ave_logitec2, int ave_logitec3, int ave_AndroidAP) {
		int strongest = 1;
		int record = ave_logitec1;
		if( record < ave_logitec2 ) {
			strongest = 2;
			record = ave_logitec2;
		}
		if( record < ave_logitec3 ) {
			strongest = 3;
			record = ave_logitec3;
		}
		if( record < ave_AndroidAP ) {
			strongest = 4;
		}
		if( ave_logitec1==0 && ave_logitec2==0 && ave_logitec3==0 && ave_AndroidAP==0 )
			strongest = 0;
		return strongest;
	}
	
	// to set IN & OUT reference number
	private static int NumberByFilter (ArrayList<Integer> alist, int signal_criterion, int referenceNumber) {
		int counter = 0;
		for(int i=0;i<referenceNumber;i++){
			if(alist.get(i) >= signal_criterion)
				counter+=1;
		}
		return counter;
	}
	
}
