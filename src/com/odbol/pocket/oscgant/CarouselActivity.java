package com.odbol.pocket.oscgant;

import java.util.ArrayList;

import com.touchmenotapps.carousel.simple.HorizontalCarouselLayout;
import com.touchmenotapps.carousel.simple.HorizontalCarouselLayout.CarouselInterface;
import com.touchmenotapps.carousel.simple.HorizontalCarouselStyle;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;

public class CarouselActivity extends Activity {
	
	private HorizontalCarouselStyle mStyle;
	private HorizontalCarouselLayout mCarousel;
	private CarouselAdapter mAdapter;
	private ArrayList<Integer> mData = new ArrayList<Integer>(0);
	private Button carousel_purchase;
	private Button carousel_back;
	private TextView theme;
	private TextView description;
	//private LinearLayout linearb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		carousel_purchase = (Button)findViewById(R.id.carousel_purchase);
		carousel_back = (Button)findViewById(R.id.carousel_back);
		theme = (TextView)findViewById(R.id.theme);
		description = (TextView)findViewById(R.id.description);
		//linearb = (LinearLayout)findViewById(R.id.linearb);
		
		theme.setBackgroundColor(Color.GRAY);
		description.setBackgroundColor(Color.GRAY);
		//linearb.setBackgroundColor(Color.GREEN);
		//carousel_purchase.setBackgroundColor(Color.GREEN);
		//carousel_back.setBackgroundColor(Color.GREEN);		
		
		/*mData.add(R.drawable.image_1);
		mData.add(R.drawable.image_3);
		mData.add(R.drawable.dishonored);
		mData.add(R.drawable.image_2);*/
		setUpmData(SemanticLP_new_sensor.playCommand);
		
		mAdapter = new CarouselAdapter(this);
		mAdapter.setData(mData);
		mCarousel = (HorizontalCarouselLayout) findViewById(R.id.carousel_layout);
		mStyle = new HorizontalCarouselStyle(this, HorizontalCarouselStyle.STYLE_LEFT_ALIGNED_ROTATION);//@@@
		mStyle.setSpaceBetweenViews(BIND_ADJUST_WITH_ACTIVITY);//@@@
		mCarousel.setBackgroundColor(Color.GRAY);
		//mCarousel.setBackgroundResource(R.drawable.p1);//@@@
		
		mCarousel.setStyle(mStyle);
		mCarousel.setAdapter(mAdapter);
				
		mCarousel.setOnCarouselViewChangedListener(new CarouselInterface() {
			@Override
			public void onItemChangedListener(View v, int position) {
				//Toast.makeText(CarouselActivity.this, "Position " + String.valueOf(position), Toast.LENGTH_SHORT).show();
			}
		});
		
		/*mCarousel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(CarouselActivity.this, "click", Toast.LENGTH_SHORT).show();
				ContinuationFragment dial = new ContinuationFragment();
				dial.show(getFragmentManager(), "tag");
			}
		});*/
		
		/*mCarousel.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(CarouselActivity.this, "long click", Toast.LENGTH_SHORT).show();
				ContinuationFragment dial = new ContinuationFragment();
				dial.show(getFragmentManager(), "tag");
				return false;
			}
		});*/
		
		carousel_purchase.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ContinuationFragment c = new ContinuationFragment();
				c.show(getFragmentManager(), "tag");
			}
		});
		
		carousel_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				CarouselActivity.this.finish();
			}
		});
		
	}
	
	//choose which set for manikin should be applied to
	void setUpmData(int number) {
		switch(number) {
		case 1:
			mData.add(R.drawable.p11);
			mData.add(R.drawable.p12);
			mData.add(R.drawable.p13);
			mData.add(R.drawable.p14);
			theme.setText("Blazer");
			description.setText("Show your outspoken style and add some fun to your look in our Blazers. Wear with a shirt and chinos for a look that exudes casual chic.");
			break;
		case 2:
			mData.add(R.drawable.p21);
			mData.add(R.drawable.p22);
			mData.add(R.drawable.p23);
			mData.add(R.drawable.p24);
			theme.setText("Shirt");
			description.setText("For those who dare to stand out is the colorful, checkered shirt available. Match your jacket with stylish chinos, a colorful shirt and suede loafers.");
			break;
		case 3:
			mData.add(R.drawable.p31);
			mData.add(R.drawable.p32);
			mData.add(R.drawable.p33);
			mData.add(R.drawable.p34);
			theme.setText("Shorts");
			description.setText("Stylish and classic men's briefs for all tastes. You will find our classic Bermuda shorts along with modern alternatives in striped or embroidered flags. Match your shorts with a colorful shirt and a pair of suede loafers and you're ready to face the summer.");
			break;
		default:
			mData.add(R.drawable.image_1);
			mData.add(R.drawable.image_3);
			mData.add(R.drawable.dishonored);
			mData.add(R.drawable.image_2);
			break;
		}
		
	}
	
	@Override
	protected void onDestroy() {
		mStyle = null;
		mCarousel = null;
		mAdapter = null;
		super.onDestroy();
		//AccData.recalibrate();
	}
	
	
	public static class ContinuationFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(
					"Thank you for your purchase! A receipt has been sent to purchase@gmail.com and your selection is now being wirelessly delivered to your device.")
					.setPositiveButton("Back to Shopping",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			return builder.create();
		}
	}
	
	/**
	 * Implementation of the vertical carousel.
	 */
	
	/*private VerticalCarouselStyle mStyle;
	private VerticalCarouselLayout mCarousel;
	private CarouselAdapter mAdapter;
	private ArrayList<Integer> mData = new ArrayList<Integer>(0);
	private Button carousel_button1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		carousel_button1 = (Button)findViewById(R.id.carousel_button1);
		
		mData.add(R.drawable.image_1);
		mData.add(R.drawable.image_3);
		mData.add(R.drawable.dishonored);
		mData.add(R.drawable.image_2);
		mAdapter = new CarouselAdapter(this);
		mAdapter.setData(mData);
		mCarousel = (VerticalCarouselLayout) findViewById(R.id.carousel_layout);
		mStyle = new VerticalCarouselStyle(this, VerticalCarouselStyle.NO_STYLE);		
		mCarousel.setStyle(mStyle);
		mCarousel.setAdapter(mAdapter);
				
		mCarousel.setOnCarouselViewChangedListener(new CarouselInterface() {
			@Override
			public void onItemChangedListener(View v, int position) {
				Toast.makeText(MainActivity.this, "Position " + String.valueOf(position), Toast.LENGTH_SHORT).show();
			}
		});
		
		carousel_button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				CarouselActivity.this.finish();
			}
		});
		
	}*/
}
