package com.odbol.pocket.oscgant;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.relivethefuture.osc.data.OscMessage;
import com.relivethefuture.osc.transport.OscClient;
import com.relivethefuture.osc.transport.OscServer;

public class SemanticLP_new_sensor extends Activity {

	static OscMessage msg = null;
	
	private SensorManager mSensorManager;
	//private AccData mSensorListener;
	static SensorFusionActivity sf;//TODO
	private static OscClient mSender;
	static String dest = "130.229.159.175";
	static String dest_poster = "130.229.160.145";//@@@ 2013-04-16
	static int port = 8080;
	private OscServer server;
	private int oscPort = 8000;
	
	public static boolean INBORDER = true;
	public static boolean judge = true;//@@@
	public static boolean screenChange = true;
	static boolean screenChange2 = true;//only check and apply to the value of rotation_vector once the blue-button is pressed
	protected static final int EDIT_PREFS = 100;
	private String phoneid = "phone1";
	private static Vibrator vibe;
	private static int milsec = 30;
	private Button white;
	private Button blue;
	public static boolean visible = false;
	
	public static int playCommand = 1;
	
	String TAG = "SemanticLP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movie_demo);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		//@@@
		initialize();
	}

	private void initialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		// reload prefs
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			oscPort = Integer.parseInt(p.getString("pref_osc_port",
					String.valueOf(oscPort)));
			port = Integer.parseInt(p.getString("pref_osc_hub_port",
					String.valueOf(port)));
			dest = p.getString("pref_osc_hub_addr1", dest);
			dest_poster = p.getString("pref_osc_hub_addr_poster", dest_poster);//@@@
			phoneid = p.getString("phoneid", phoneid);
		} catch (Exception e) {
			Toast.makeText(this, "Invalid data in preferences", Toast.LENGTH_LONG);
		}
		mSender = new OscClient(true);
		//mSensorListener = new AccData(mSender, dest, port, phoneid);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		
		sf = new SensorFusionActivity(mSensorManager,mSender, dest, port, phoneid);//TODO
		
		vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

		white = (Button) findViewById(R.id.white);
		blue = (Button) findViewById(R.id.blue);
		
		//white.setBackgroundResource(R.drawable.gant_big_button);
		//blue.setBackgroundResource(R.drawable.gant_small_button);
		white.setBackgroundResource(R.drawable.big);//for gant
		blue.setBackgroundResource(R.drawable.small);//for gant

		white.setOnTouchListener(new ButtonTouch());
		blue.setOnTouchListener(new ButtonTouch());
		startListening(); 
	}
	
	public class ButtonTouch implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				String text = "none";
				switch (v.getId()) {
				case R.id.white:
					text = "/" + phoneid + "/bigbutton";
					break;
				case R.id.blue:
					screenChange2 = screenChange;
					
					if(screenChange2) {
						white.setBackgroundResource(R.drawable.big_button);
						blue.setBackgroundResource(R.drawable.small_button);
						}
					else {
						white.setBackgroundResource(R.drawable.gant_big_button);
						blue.setBackgroundResource(R.drawable.gant_small_button);
						}
					
					Log.v("test", "blue clicked " + screenChange2);
					text = "/" + phoneid + "/bluebutton";
					//AccData.recalibrate(); //@@@ reset the position of X and Y
					SensorFusionActivity.recalibrate();//TODO
					break;
				}
				//@@@ IpListener
				if (!text.equals("none")) {
					vibe.vibrate(milsec);
					mSender.connect(new InetSocketAddress(dest, port));
					
					if(INBORDER) {
						if(screenChange2) {//judge which server need to be connected
							mSender.disconnect();
							mSender.connect(new InetSocketAddress(dest, port));}
						else if(!screenChange2) {
							mSender.disconnect();
							mSender.connect(new InetSocketAddress(dest_poster, port)); }
						
						try {
							msg = new OscMessage(text);
							msg.addArgument("/"+phoneid);//patcher style
							//msg.addArgument(1);//MAXX style
							mSender.sendPacket(msg);
							
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					else { mSender.disconnect(); }
					
				} else if (text.equals("none")) {
					Log.v("test", "test");
				}
			}

			return false;
		}

	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		/*mSensorManager.registerListener(mSensorListener,mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
				SensorManager.SENSOR_DELAY_GAME);
		mSensorManager.registerListener(mSensorListener,mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_GAME);*/
		startListening();
		
		sf.Stop();
		sf = new SensorFusionActivity(mSensorManager,mSender, dest, port, phoneid);//TODO
		
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		//mSensorManager.unregisterListener(mSensorListener2);
		//stopListening();
		//sf.Stop();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		//mSensorManager.unregisterListener(mSensorListener);
		stopListening();
		sf.Stop();
		super.onDestroy();
	}

	@Override
	public void finish() {
		//mSensorManager.unregisterListener(mSensorListener);
		stopListening();
		super.finish();
	}

	private void startListening() {
		stopListening(); 

		try {
			server = new OscServer(oscPort);
			server.setUDP(true);
			server.start();
		} catch (Exception e) {
			Toast.makeText(this,
					"Failed to start OSC server: " + e.getMessage(),
					Toast.LENGTH_LONG);
			return;
		}
		server.addOscListener(new LooperListener_new_sensor(this));
	}

	private void stopListening() {
		if (server != null) {
			server.stop();
			server = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.tab, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.prefs:
			Intent intent = new Intent(this, OSCTesterClientPreferences.class);
			startActivityForResult(intent, EDIT_PREFS);
			return true;
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDIT_PREFS)
			initialize();
		else if (requestCode!=EDIT_PREFS ) {
			Log.v("test","request code "+requestCode);
			if(screenChange2)
				mSender.connect(new InetSocketAddress(dest, port));
			else
				mSender.connect(new InetSocketAddress(dest_poster, port));
			Log.v("test","received stop video"+requestCode);
			try {
				OscMessage msg = new OscMessage("/stopvideo");
				msg.addArgument(requestCode);
				mSender.sendPacket(msg);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void stopvideo(int vidno){
		Log.v("test","request code "+vidno);
		if(screenChange2)
			mSender.connect(new InetSocketAddress(dest, port));
		else
			mSender.connect(new InetSocketAddress(dest_poster, port));
		Log.v("test","received stop video"+vidno);
		try {
			OscMessage msg = new OscMessage("/stopvideo");
			msg.addArgument(vidno);
			mSender.sendPacket(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@SuppressLint("ValidFragment")
	public static class PopUpFragment extends DialogFragment {
		
		String value_arg;
		ImageView image;
		Bitmap bitmap;
		public PopUpFragment(String val) {
			value_arg = val;
		}

		public void setValue_arg(String value_arg) {
			this.value_arg = value_arg;
		}

		@Override
		public void show(FragmentManager manager, String tag) {
			visible = true;
			super.show(manager, tag);
		}


		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();
			ViewGroup vg = (ViewGroup)inflater.inflate(R.layout.popup, null);
	        image= (ImageView) vg.findViewById(R.id.image);
	        
	        int ratio = 720 / 720;
	     BitmapFactory.Options options = new BitmapFactory.Options();
	     options.inSampleSize = ratio;
	     
	     //gant
	     if(Integer.parseInt(value_arg)<1 || Integer.parseInt(value_arg)>20)
	    	 value_arg="20";
	     
	     //gant
	     //Uri uri = null;
	     //if(screenChange2) //judge which server need to be connected
	    //	 uri = Uri.parse("android.resource://"+this.getActivity().getPackageName()+"/drawable/g"+value_arg);
	     //else if(!screenChange2)
	    //	 uri = Uri.parse("android.resource://"+this.getActivity().getPackageName()+"/drawable/p"+value_arg);
	     Uri uri = Uri.parse("android.resource://"+this.getActivity().getPackageName()+"/drawable/g"+value_arg);//gant drawable/p
	     
	     InputStream inputStream = null;
		try {
			inputStream = this.getActivity().getContentResolver().openInputStream(uri);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	     bitmap = BitmapFactory.decodeStream(inputStream, null, options);
	     
	        image.setImageBitmap(bitmap);
	        //image.setImageURI(uri);
			builder.setView(vg);
					builder/*.setPositiveButton("View Trailer",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									visible = false;
									//Intent i = LOoperListener.getVideoIntent("/sdcard/SmartMob/"+value_arg+".mp4");
									//getActivity().startActivityForResult(i, Integer.parseInt(value_arg));
								}
							})
					*/.setNegativeButton("Purchase",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									visible = false;
									ContinuationFragment dial = new ContinuationFragment();
									dial.show(getFragmentManager(), "tag");
									//stopvideo(Integer.parseInt(value_arg));
								}
							})
					.setNeutralButton("Back",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									visible = false;
									//stopvideo(Integer.parseInt(value_arg));
								}
							});
					vibe.vibrate(milsec);
			return builder.create();
		}
		@Override
		public void onLowMemory() {
			System.gc();
			Runtime.getRuntime().gc(); 
		};

		@Override
		public void onDestroyView() {
			System.gc();
			Runtime.getRuntime().gc(); 
//			Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
			if(!bitmap.isRecycled()){
			bitmap.recycle();
			bitmap =null;}
			super.onDestroyView();
		}
	}
	
	//**
	public static class PopUpFragmentAlarm extends DialogFragment {
		
		public PopUpFragmentAlarm() {
		}
		@Override
		public void show(FragmentManager manager, String tag) {
			super.show(manager, tag);
		}
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("full");
			builder.setMessage("Full of registered terminals, please try later. ");
			
			builder.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					
				}
				});
			return builder.create();
		}
		@Override
		public void onDestroyView() {
			System.gc();
			Runtime.getRuntime().gc(); 
			super.onDestroyView();
		}
	}
	//***

	@SuppressLint("ValidFragment")
	public static class ContinuationFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(
					"Thank you for your purchase! A receipt has been sent to ajohansson@gmail.com and your selection is now being wirelessly delivered to your device.")
					.setPositiveButton("Back to Shopping",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			return builder.create();
		}
	}
	
	public static void sendXY(float x_pointer, float y_pointer) {
			msg = new OscMessage("/phone1"); // patcher style
			msg.addArgument(x_pointer / 100f);// divide it by 100
			msg.addArgument(y_pointer / 100f);
			try {
				mSender.sendPacket(msg);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

}


