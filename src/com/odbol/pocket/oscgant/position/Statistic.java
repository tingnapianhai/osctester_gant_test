/*
 * Terminal information 
 * Network side: network connection, signal strength, base station cell-id & lac, wifi status & bssid, network connection status
 * Terminal side: terminal model, position, GPS status, battery level, screen status, date time, location for GPS & network
 * Service side: foreground application, active application, data sent/received by active/all app, positioning service accuracy
 * */
package com.odbol.pocket.oscgant.position;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class Statistic{
	
private static final String TAG = "Statistic";
	
	public static double[] phone_angle = new double[3];// to show the phone's angle
	
	private static String Pietro_phone = "AndroidAP";
	public static boolean detect_Pietro_phone = false;
	public static boolean Pietro_phone_INOUT = false;
	
	public static int strongestAP = 0;
	public static int strongestAP_tplink = 0;
	public static int strongestAP_jointRouter = 0;
	public static String strongestAPname = " ";
	public static int average_logitec1 = 0;
	public static int average_logitec2 = 0;
	public static int average_logitec3 = 0;
	public static int average_tplink1 = 0;
	public static int average_tplink2 = 0;
	public static int average_pietro_phone = 0;
	
	private static final String Logitec1 = "00:01:8e:99:0d:00"; //logitec: 1
	private static final String Logitec2 = "00:01:8e:99:8d:e2"; //logitec: 2
	private static final String Logitec3 = "00:01:8e:99:8e:5c"; //logitec: 3
	private static final String TPLink1 = "a0:f3:c1:9a:47:1c"; //TP-LINK: 1***
	private static final String TPLink2 = "90:f6:52:71:8b:5b"; //TP-LINK: 2***
	public static int Logitec1_level = -100;
	public static int Logitec2_level = -100;
	public static int Logitec3_level = -100;
	public static int TPLink1_level = -100;
	public static int TPLink2_level = -100;
	public static int Pietro_level = -100;
	public static ArrayList<Integer> filter_logitec1 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_logitec2 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_logitec3 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_tplink1 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_tplink2 = initializeArrayList2(-100,-100,-100,-100,-100);
	public static ArrayList<Integer> filter_pietro_phone = initializeArrayList2(-100,-100,-100,-100,-100);
	//public static int counterINreceiver = 0;
	
	public static int counter_statistic_auto = 0;
	public static int point_number_auto = 0; //kind of useless
	public static int point_number_auto_logitec = 0;
	public static int point_number_auto_tplink = 0;
	public static int point_number_auto_jointRouter = 0;
	
	public static int phonesignal = -1;
	public static int phonesignal_dBm = -1;//undo
	public static int battery_voltage = -1;
	public static int battery_level = -1;
	public static int battery_status = -1;
	public static float battery_temperature = -1;
	
	public static double GPS_Latitude = 0;
	public static double GPS_Longitude = 0;
	public static float GPS_Accuracy_API = 0;
	public static double Network_Latitude = 0;
	public static double Network_Longitude = 0;
	public static float Network_Accuracy_API = 0;
	public static int GPS_First_FixTime = 0;
	public static int Satellite_Using = 0;
	public static int Satellite_Visible = 0;
	
	public static ArrayList<Integer> initializeArrayList (int AP1, int AP2, int AP3)
	{
		ArrayList<Integer> alist = new ArrayList<Integer> ();
		alist.add(0, AP1);
		alist.add(1, AP2);
		alist.add(2, AP3);
		return alist;
	}
	
	public static ArrayList<Integer> initializeArrayList2 (int AP1, int AP2, int AP3, int AP4, int AP5)
	{
		ArrayList<Integer> alist = new ArrayList<Integer> ();
		alist.add(0, AP1);
		alist.add(1, AP2);
		alist.add(2, AP3);
		alist.add(3, AP4);
		alist.add(4, AP5);
		return alist;
	}
	
	//****************************************************************************************
	//****************************************************************************************
	//****************************************************************************************
	//if not initialize the Logitec1_level(Logitec2_level, etc..), if there is no logitec2, the level-value of logitec2 will not change, but remain/stay in the previous level-value
	public static void getIndoorPositionReferenceAuto_3Logitec (WifiManager wifi) {
		
		boolean detect_logitec1 = false; //to detect if "logitec1 AP" can be detected by phone, which means if the "logitec1 AP" is visible or not
		boolean detect_logitec2 = false;
		boolean detect_logitec3 = false;
		boolean detect_AP = false; // to detect Pietro_phone
		boolean detect_tplink1 = false;
		boolean detect_tplink2 = false;
		
		wifi.startScan();
		
		List<ScanResult> mWifiList = wifi.getScanResults();
		ScanResult sr;
		int i = 0;
		if(mWifiList!=null) {
			int size = mWifiList.size();
			for(i=0; i < size; i++) {
				sr = mWifiList.get(i);
				if(sr.BSSID.equalsIgnoreCase(Logitec1)) {
					Logitec1_level = sr.level;
					filter_logitec1.set(4, filter_logitec1.get(3));
					filter_logitec1.set(3, filter_logitec1.get(2));
					filter_logitec1.set(2, filter_logitec1.get(1));
					filter_logitec1.set(1, filter_logitec1.get(0));
					filter_logitec1.set(0, Logitec1_level);
					detect_logitec1 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec2)) {
					Logitec2_level = sr.level;
					filter_logitec2.set(4, filter_logitec2.get(3));
					filter_logitec2.set(3, filter_logitec2.get(2));
					filter_logitec2.set(2, filter_logitec2.get(1));
					filter_logitec2.set(1, filter_logitec2.get(0));
					filter_logitec2.set(0, Logitec2_level);
					detect_logitec2 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec3)) {
					Logitec3_level = sr.level;
					filter_logitec3.set(4, filter_logitec3.get(3));
					filter_logitec3.set(3, filter_logitec3.get(2));
					filter_logitec3.set(2, filter_logitec3.get(1));
					filter_logitec3.set(1, filter_logitec3.get(0));
					filter_logitec3.set(0, Logitec3_level);
					detect_logitec3 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink1)) {
					TPLink1_level = sr.level;
					filter_tplink1.set(4, filter_tplink1.get(3));
					filter_tplink1.set(3, filter_tplink1.get(2));
					filter_tplink1.set(2, filter_tplink1.get(1));
					filter_tplink1.set(1, filter_tplink1.get(0));
					filter_tplink1.set(0, TPLink1_level);
					detect_tplink1 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink2)) {
					TPLink2_level = sr.level;
					filter_tplink2.set(4, filter_tplink2.get(3));
					filter_tplink2.set(3, filter_tplink2.get(2));
					filter_tplink2.set(2, filter_tplink2.get(1));
					filter_tplink2.set(1, filter_tplink2.get(0));
					filter_tplink2.set(0, TPLink2_level);
					detect_tplink2 = true;
				}
				
				// detect Pietro_phone AndroidAP;
				else if(sr.SSID.equalsIgnoreCase(Pietro_phone)) {
					Pietro_level = sr.level;
					filter_pietro_phone.set(4, filter_pietro_phone.get(3));
					filter_pietro_phone.set(3, filter_pietro_phone.get(2));
					filter_pietro_phone.set(2, filter_pietro_phone.get(1));
					filter_pietro_phone.set(1, filter_pietro_phone.get(0));
					filter_pietro_phone.set(0, Pietro_level);
					detect_AP = true;
				}
			} // end for
		} //end if
		
		detect_Pietro_phone = detect_AP; // to show the Pietro_phone status
		
		Logitec1_level = ArrayListConvert(filter_logitec1, detect_logitec1, Logitec1_level);
		Logitec2_level = ArrayListConvert(filter_logitec2, detect_logitec2, Logitec2_level);
		Logitec3_level = ArrayListConvert(filter_logitec3, detect_logitec3, Logitec3_level);
		TPLink1_level = ArrayListConvert(filter_tplink1, detect_tplink1, TPLink1_level);
		TPLink2_level = ArrayListConvert(filter_tplink2, detect_tplink2, TPLink2_level);
		Pietro_level = ArrayListConvert(filter_pietro_phone, detect_AP, Pietro_level);
		
	}
	
	//convert ArrayList
	public static int ArrayListConvert (ArrayList<Integer> alist, boolean detect, int instant)
	{
		if(!detect) {
			alist.set(4, alist.get(3));
			alist.set(3, alist.get(2));
			alist.set(2, alist.get(1));
			alist.set(1, alist.get(0));
			alist.set(0, -100);
			instant = -100;
		}
		return instant;
	}
	
	// hard in, harder out
	public static String getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi) {
		
		getIndoorPositionReferenceAuto_3Logitec (wifi);
		
		Monitor.Monitor_filterINnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		
		average_logitec1 = AverageSignalStrength(filter_logitec1, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec2 = AverageSignalStrength(filter_logitec2, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec3 = AverageSignalStrength(filter_logitec3, Monitor.Monitor_filterINOUTreferenceNumber);
		average_pietro_phone = AverageSignalStrength(filter_pietro_phone, Monitor.Monitor_filterINOUTreferenceNumber);
		strongestAP = ComparisonSignalStrength(average_logitec1,average_logitec2,average_logitec3,average_pietro_phone);
		
		// remember to add "return" statement ----------------------------------------------------
		int test = testStrongestFirst();
		if(test>0)
			return " " + test;
		
		switch (point_number_auto_logitec) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 0:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber) >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			break;
		// in Point 1
		case 1:
			if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			//else if(NumberByFilter(filter_logitec1, filterOut)>0 )
			else if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			break;
		// in Point 2
		case 2:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber)
			{
				point_number_auto_logitec = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return "AndroidAP";
			}
			
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_logitec = 2;
				return "logitec 2";
			}
			break;
		// in Point 3
		case 3:
				if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 1;
					return "logitec 1";
				}
				else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 2;
					return "logitec 2";
				}
				else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 4;
					return "AndroidAP";
				}
				
				else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_logitec = 3;
					return "logitec 3";
				}
				break;
		// in AndroidAP
		case 4:
				if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 1;
					return "logitec 1";
				}
				else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 2;
					return "logitec 2";
				}
				else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_logitec = 3;
					return "logitec 3";
				}
				
				else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_logitec = 4;
					return "AndroidAP";
				}
				break;
			
		default: break;
		}// end switch
		
		point_number_auto_logitec = 0;
		
		return "no ";
	} // end getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi)
	
	// 2013-03-28 judge the TPLink1&2  and  Logitec1,2,3,AndroidAP separately. 
	// **************** hard in, harder out
	public static String getButtonNumberAuto_2TPLinkFilter_InOut (WifiManager wifi) {
		getIndoorPositionReferenceAuto_3Logitec (wifi);
				
		Monitor.Monitor_filterINnumber_tplink1 = NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_tplink1 = NumberByFilter(filter_tplink1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_tplink2 = NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_tplink2 = NumberByFilter(filter_tplink2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
				
		average_tplink1 = AverageSignalStrength(filter_tplink1, Monitor.Monitor_filterINOUTreferenceNumber);
		average_tplink2 = AverageSignalStrength(filter_tplink2, Monitor.Monitor_filterINOUTreferenceNumber);
				
		strongestAP_tplink = ComparisonSignalStrength(average_tplink1,average_tplink2);
				
		// remember to add "return" statement ----------------------------------------------------
		int test = testStrongestFirst_TPLink(); // 2013-03-28 with new router - TPLink
		if(test>0)
			return " " + test;
				
		switch (point_number_auto_tplink) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 0:
			if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber) >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_tplink = 1;
				return "tplink 1";
				}
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_tplink = 2;
				return "tplink 2";
				}
			break;
			// in Point 1
			case 1:
				if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_tplink = 2;
					return "tplink 2";
					}
				//else if(NumberByFilter(filter_logitec1, filterOut)>0 )
				else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_tplink = 1;
					return "tplink 1";
					}
				break;
				// in Point 2
			case 2:
				if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber)
				{
					point_number_auto_tplink = 1;
					return "tplink 1";
					}
					
				else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_tplink = 2;
					return "tplink 2";
					}
				break;
					
			default: break;
			}// end switch
				
		point_number_auto_tplink = 0;
				
		return "no ";
		} // end getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi)
	
	// hard in, harder out
	// 2013-04-03 Joint with logitec 1,2,3 & AndroidAP & TPLink1,2
	public static String getButtonNumberAuto_JointRouterFilter_InOut (WifiManager wifi) {
		
		// 2013-04-03, already calculated by other two, block this to fasten the speed
/*			getIndoorPositionReferenceAuto_3Logitec (wifi);
		Monitor.Monitor_filterINnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec1 = NumberByFilter(filter_logitec1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec2 = NumberByFilter(filter_logitec2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_logitec3 = NumberByFilter(filter_logitec3, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_pietro_phone = NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_tplink1 = NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_tplink1 = NumberByFilter(filter_tplink1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterINnumber_tplink2 = NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber);
		Monitor.Monitor_filterOUTnumber_tplink2 = NumberByFilter(filter_tplink2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber);
		
		average_logitec1 = AverageSignalStrength(filter_logitec1, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec2 = AverageSignalStrength(filter_logitec2, Monitor.Monitor_filterINOUTreferenceNumber);
		average_logitec3 = AverageSignalStrength(filter_logitec3, Monitor.Monitor_filterINOUTreferenceNumber);
		average_pietro_phone = AverageSignalStrength(filter_pietro_phone, Monitor.Monitor_filterINOUTreferenceNumber);
		average_tplink1 = AverageSignalStrength(filter_tplink1, Monitor.Monitor_filterINOUTreferenceNumber);
		average_tplink2 = AverageSignalStrength(filter_tplink2, Monitor.Monitor_filterINOUTreferenceNumber);*/
		
		strongestAP_jointRouter = ComparisonSignalStrength(average_logitec1,average_logitec2,average_logitec3,average_pietro_phone,average_tplink1,average_tplink2);
		
		// remember to add "return" statement ----------------------------------------------------
		int test = testStrongestFirst_jointRouter();
		if(test>0)
			return " " + test;
		
		switch (point_number_auto_jointRouter) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 0:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber) >= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return "AndroidAP";
			}
			else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 5;
				return "TPLink 1";
			}
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 6;
				return "TPLink 2";
			}
			break;
		// in Point 1
		case 1:
			if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return "AndroidAP";
			}
			else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 5;
				return "TPLink 1";
			}
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 6;
				return "TPLink 2";
			}
			
			//else if(NumberByFilter(filter_logitec1, filterOut)>0 )
			else if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_jointRouter = 1;
				return "logitec 1";
			}
			break;
		// in Point 2
		case 2:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber)
			{
				point_number_auto_jointRouter = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return "AndroidAP";
			}
			else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 5;
				return "TPLink 1";
			}
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 6;
				return "TPLink 2";
			}
			
			//
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_jointRouter = 2;
				return "logitec 2";
			}
			break;
		// in Point 3
		case 3:
				if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 1;
					return "logitec 1";
				}
				else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 2;
					return "logitec 2";
				}
				else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 4;
					return "AndroidAP";
				}
				else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 5;
					return "TPLink 1";
				}
				else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 6;
					return "TPLink 2";
				}
				
				//
				else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_jointRouter = 3;
					return "logitec 3";
				}
				break;
		// in AndroidAP
		case 4:
				if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 1;
					return "logitec 1";
				}
				else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 2;
					return "logitec 2";
				}
				else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 3;
					return "logitec 3";
				}
				else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 5;
					return "TPLink 1";
				}
				else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
				{
					point_number_auto_jointRouter = 6;
					return "TPLink 2";
				}
				
				//
				else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
				{
					point_number_auto_jointRouter = 4;
					return "AndroidAP";
				}
				break;
		
		// in TPLink 1
		case 5:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return "AndroidAP";
			}
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 6;
				return "TPLink 2";
			}
			
			//
			else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_jointRouter = 5;
				return "TPLink 1";
			}
			break;
		
		// in TPLink 2
		case 6:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 1;
				return "logitec 1";
			}
			else if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 2;
				return "logitec 2";
			}
			else if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return "logitec 3";
			}
			else if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return "AndroidAP";
			}
			else if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 5;
				return "TPLink 1";
			}
			
			//
			else if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterOut, Monitor.Monitor_filterINOUTreferenceNumber)>0 )
			{
				point_number_auto_jointRouter = 6;
				return "TPLink 2";
			}
			break;
				
		default: break;
		}// end switch
		
		point_number_auto_jointRouter = 0;
		
		return "no ";
	} // end getButtonNumberAuto_3LogitecFilter_InOut (WifiManager wifi)
	
	// try/test the AP with strongest average-signal-strength, to check if this AP meet the requirement to connect/register on the system
	// only logitec 1,2,3 & AndroidAP
	private static int testStrongestFirst () {
		//ArrayList< ArrayList<Integer> > alist_number = new ArrayList< ArrayList<Integer> > ();
		switch (strongestAP) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 1:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 1;
				return 1;
			}
		case 2:
			if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 2;
				return 2;
			}
		case 3:
			if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 3;
				return 3;
			}
		case 4:
			if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_logitec = 4;
				return 4;
			}
		default: return 0;
		}
	}
	
	// try/test the AP with strongest average-signal-strength, to check if this AP meet the requirement to connect/register on the system
	private static int testStrongestFirst_TPLink () {
		//ArrayList< ArrayList<Integer> > alist_number = new ArrayList< ArrayList<Integer> > ();
		switch (strongestAP_tplink) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 1:
			if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_tplink = 1;
				return 1;
			}
		case 2:
			if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_tplink = 2;
				return 2;
			}
		default: return 0;
		}
	}
	
	// try/test the AP with strongest average-signal-strength, to check if this AP meet the requirement to connect/register on the system
	//2013-04-03 
	private static int testStrongestFirst_jointRouter () {
		//ArrayList< ArrayList<Integer> > alist_number = new ArrayList< ArrayList<Integer> > ();
		switch (strongestAP_jointRouter) {
		// if "no point", which means the terminal is "OUT" of AP.
		case 1:
			if(NumberByFilter(filter_logitec1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 1;
				return 1;
			}
		case 2:
			if(NumberByFilter(filter_logitec2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 2;
				return 2;
			}
		case 3:
			if(NumberByFilter(filter_logitec3, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 3;
				return 3;
			}
		case 4:
			if(NumberByFilter(filter_pietro_phone, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 4;
				return 4;
			}
		case 5:
			if(NumberByFilter(filter_tplink1, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 5;
				return 5;
			}
		case 6:
			if(NumberByFilter(filter_tplink2, Monitor.Monitor_filterIn, Monitor.Monitor_filterINOUTreferenceNumber)>= Monitor.Monitor_filterINOUTreferenceNumber )
			{
				point_number_auto_jointRouter = 6;
				return 6;
			}
		default: return 0;
		}
	}
	
	// to calculate the average-signal-strength for specific Access-Point
	private static int AverageSignalStrength (ArrayList<Integer> alist, int referenceNumber) {
		int sum = 0;
		int ave = 0;
		for(int i=0;i<referenceNumber;i++){
			sum += alist.get(i);
		}
		ave = sum/referenceNumber;
		return ave;
	}
	
	// to compare average signal-strength
	private static int ComparisonSignalStrength (int ave_logitec1, int ave_logitec2, int ave_logitec3, int ave_AndroidAP) {
		int strongest = 1;
		int record = ave_logitec1;
		if( record < ave_logitec2 ) {
			strongest = 2;
			record = ave_logitec2;
		}
		if( record < ave_logitec3 ) {
			strongest = 3;
			record = ave_logitec3;
		}
		if( record < ave_AndroidAP ) {
			strongest = 4;
		}
		if( ave_logitec1==0 && ave_logitec2==0 && ave_logitec3==0 && ave_AndroidAP==0 )
			strongest = 0;
		return strongest;
	}
	
	// to compare average signal-strength
	private static int ComparisonSignalStrength (int ave_tplink1, int ave_tplink2) {
		int strongest = 1;
		int record = ave_tplink1;
		if( record < ave_tplink2 ) {
			strongest = 2;
		}
		if( ave_tplink1==0 && ave_tplink2==0 )
			strongest = 0;
		return strongest;
	}
	
	// to compare average signal-strength
	private static int ComparisonSignalStrength (int ave_logitec1, int ave_logitec2, int ave_logitec3, int ave_AndroidAP, int ave_tplink1, int ave_tplink2) {
		int strongest = 1;
		int record = ave_logitec1;
		if( record < ave_logitec2 ) {
			strongest = 2;
			record = ave_logitec2;
		}
		if( record < ave_logitec3 ) {
			strongest = 3;
			record = ave_logitec3;
		}
		if( record < ave_AndroidAP ) {
			strongest = 4;
			record = ave_AndroidAP;
		}
		if( record < ave_tplink1 ) {
			strongest = 5;
			record = ave_tplink1;
		}
		if( record < ave_tplink2 ) {
			strongest = 6;
			record = ave_tplink2;
		}
		
		if( ave_logitec1==0 && ave_logitec2==0 && ave_logitec3==0 && ave_AndroidAP==0 && ave_tplink1==0 && ave_tplink2==0 )
			strongest = 0;
		return strongest;
	}
	
	// to set IN & OUT reference number
	private static int NumberByFilter (ArrayList<Integer> alist, int signal_criterion, int referenceNumber) {
		int counter = 0;
		for(int i=0;i<referenceNumber;i++){
			if(alist.get(i) >= signal_criterion)
				counter+=1;
		}
		return counter;
	}
		
		// to see what is the number to meet requirement. for instance, 3 out of 5
		private static int NumberByFilter (ArrayList<Integer> alist, int signal_criterion) {
			int counter = 0;
			for(int i=0;i<5;i++){
				if(alist.get(i) >= signal_criterion)
					counter+=1;
			}
			return counter;
		}		
		//****************************************************************************************
		//****************************************************************************************
		//****************************************************************************************
		
/*		public static int getPointNumberAuto () {
			return point_number_auto;
		}
	
	public static int getUserPointNumber() {
		if(!Monitor.pointselectstatus_1 && !Monitor.pointselectstatus_2 && !Monitor.pointselectstatus_3)
			return 0;
		else if(Monitor.pointselectstatus_1)
			return 1;
		else if(Monitor.pointselectstatus_2)
			return 2;
		else if(Monitor.pointselectstatus_3)
			return 3;
		else
			return 0;
	}
	
	//******************************************
	
	public static String getDateTime() {
		Date date = new java.util.Date();
		Timestamp ts = new java.sql.Timestamp(date.getTime());
		return ts.toString();
	}*/
	
}
