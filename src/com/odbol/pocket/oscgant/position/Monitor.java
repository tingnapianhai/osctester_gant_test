/*
 * 1 onStart - setForeground(true);
 * 2 onStart - PowerManager pm = null; WakeLock wakeLock = null; 
 * 3 run() -  android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_DISPLAY);
 * 4 AndroidManifest.xml - android:persistent="true" 
 * If still not working, use 
 * 1 AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
 *    am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 5*1000, sender);
 * 2 handler or
 * 3 Executors.newScheduledThreadPool or ScheduledThreadPoolExecutor or
 * 4 trigger
 * */
package com.odbol.pocket.oscgant.position;

public class Monitor {
	
	static final int MONITOR_FREQUENCY = 1*1000;
	static final int MONITOR_DURATION = 3*24*3600*1000;
	public static int Monitor_filterOut = -60; // standard, the value get the terminal OUT the AP
	public static int Monitor_filterIn = -55; // standard, the value get the terminal IN the AP
	//public static int Monitor_filterINOUTmargin = 0; // the margin value between IN value and OUT value, coefficient
	public static int Monitor_filterINOUTreferenceNumber = 2; // standard, the minimal number for connection part, IN
	
	public static int Monitor_filterINnumber_logitec1 = 0; //the actual IN number of samples out of 5
	public static int Monitor_filterOUTnumber_logitec1 = 0;
	public static int Monitor_filterINnumber_logitec2 = 0;
	public static int Monitor_filterOUTnumber_logitec2 = 0;
	public static int Monitor_filterINnumber_logitec3 = 0;
	public static int Monitor_filterOUTnumber_logitec3 = 0;
	public static int Monitor_filterINnumber_pietro_phone = 0;//show pietro's AP IN number
	public static int Monitor_filterOUTnumber_pietro_phone = 0;//show pietro's AP OUT number
	public static int Monitor_filterINnumber_tplink1 = 0;
	public static int Monitor_filterOUTnumber_tplink1 = 0;
	public static int Monitor_filterINnumber_tplink2 = 0;
	public static int Monitor_filterOUTnumber_tplink2 = 0;
	
	static public boolean pointselectstatus_1 = false;
	static public boolean pointselectstatus_2 = false;
	static public boolean pointselectstatus_3 = false;
	static public boolean runningStatus;
    
}
