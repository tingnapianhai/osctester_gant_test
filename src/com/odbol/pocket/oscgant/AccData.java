package com.odbol.pocket.oscgant;

import java.net.InetSocketAddress;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.odbol.pocket.oscgant.algos.Algo;
import com.odbol.pocket.oscgant.algos.BufferAlgo2;
import com.relivethefuture.osc.data.OscMessage;
import com.relivethefuture.osc.transport.OscClient;

public class AccData implements SensorEventListener {

	OscMessage msg = null;
	OscClient sender;
	InetSocketAddress addr;
	String phoneid;
	private float[] orie_prev = new float[3];//prev angles
	private static float[] orie = new float[3];//cur angles
	private static float[] orient = new float[3];//new
	float maxangle = 10.00f;
	float dist_maxangle = 56.00f;
	float maxdiffangle = 10.00f;
	float maxx = 10;
	float minx = -10;
	float maxy = 10;
	float miny = -10;
	private static float x_pointer = 0;
	private static float y_pointer = 0;
	float x =0;
	float y=0;
	int listMaxSize = 10;
	static float xaxis =0 ;
	static float yaxis =0;
	public Algo orientationBufferAlgo;
	boolean first = true;
	boolean dist_first = true;
	
	private static final float NS2S = 1.0f / 1000000000.0f;
	private static float angle[] = {0.0f,0.0f,0.0f};
	private static float angle_pointer[] = {0.0f,0.0f,0.0f};
	private static float timestamp;
	int angle_x = 0;
	int angle_y = 0;

	public AccData(OscClient oscClient, String destination, int destPort, String id) {
		sender = oscClient;
		addr = new InetSocketAddress(destination, destPort);
		sender.connect(addr);
		phoneid = id;
	}
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}

	public static void recalibrate(){
	//SensorManager.getRotationMatrixFromVector(rot_new, orient);
	angle[0] = 0.0f;
	angle[2] = 0.0f;
	}
	
	@Override
	public void onSensorChanged(SensorEvent se) {
		orientationBufferAlgo = new BufferAlgo2(0.55f); //previous value:1.0f; pointer calibration
		orientationBufferAlgo.execute(orient, se.values.clone(), 0.0f);
		orie_prev[0] = orie[0];// assign previos angles
		orie_prev[1] = orie[1];
		orie[0] = (float) (2*Math.asin(se.values[2])); //  compute curr angle for x 
		orie[1] = (float) (2*Math.asin(se.values[0])); // curr angle for y
		
		// -100 ig lab
		//black-1 >-50, <-50, to judge which server need to be connected
		if (se.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
			double m = 2*Math.asin(se.values[2]);
			double z = Math.toDegrees(m);
			if(SemanticLP.judge && z>-60 && z<85) {
				//sender.disconnect();
				//sender.connect(addr2);
				SemanticLP.judge = false;
				SemanticLP.screenChange = false;
			}
			if(!SemanticLP.judge && (z<-60 || z>100)) {
				//sender.disconnect();
				//sender.connect(addr1);
				SemanticLP.judge = true;
				SemanticLP.screenChange = true;
			}
		}
		
		if (se.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
			if(timestamp != 0) {
				final float dT = (se.timestamp - timestamp) * NS2S;
				angle_x = (int) (se.values[2]*100);//take two digits after decimal amount, multiply by 100, convert to Integer
				angle_y = (int) (se.values[0]*100);
				angle[2] = angle[2] + angle_x * dT;
				angle[0] = angle[0] + angle_y * dT;
				angle_pointer[0] = 0.3f * 26*angle[0] + 0.7f * angle_pointer[0];//26 is the coefficient to covert to coordinate
				angle_pointer[2] = 0.3f * 26*angle[2] + 0.7f * angle_pointer[2];//26 is the coefficient to covert to coordinate
				}
			timestamp = se.timestamp;
			x_pointer = angle_pointer[2] * (-1);
			y_pointer = angle_pointer[0];
			msg = new OscMessage("/" + phoneid ); // patcher style
			msg.addArgument(x_pointer/100f);//divide it by 100
			msg.addArgument(y_pointer/100f);
			try {
				sender.sendPacket(msg);
				} catch (InterruptedException e) {
					e.printStackTrace();
					}
			}
		
	}

}
