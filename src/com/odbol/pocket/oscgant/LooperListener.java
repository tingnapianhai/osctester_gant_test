package com.odbol.pocket.oscgant;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.odbol.pocket.oscgant.SemanticLP.PopUpFragment;
import com.odbol.pocket.oscgant.SemanticLP.PopUpFragmentAlarm;
import com.relivethefuture.osc.data.BasicOscListener;
import com.relivethefuture.osc.data.OscMessage;

public class LooperListener extends BasicOscListener {
	
	private Activity c;

	public LooperListener(Activity appActivity) {
		c = appActivity;
	}

	@Override
	public void handleMessage(OscMessage msg) {
		
		String address = msg.getAddress();
		
		//command /play
		if(address.equals("/play")) {
			SemanticLP.playCommand = Integer.parseInt(msg.getArguments().get(0).toString());
			Intent it = new Intent(c.getApplicationContext(), CarouselActivity.class);
			c.startActivity(it);
			}
		//command /playvideo
		else if(address.equals("/playvideo")) {
			if(!SemanticLP.visible){
				String val = msg.getArguments().get(0).toString();
				PopUpFragment dial = new PopUpFragment(val);
				dial.show(c.getFragmentManager(), "tag");
			}
			else {
				SemanticLP.stopvideo(Integer.parseInt(msg.getArguments().get(0).toString()));
				}
			}
		
		else if(address.equals("/full")) {
			PopUpFragmentAlarm dial = new PopUpFragmentAlarm();
			dial.show(c.getFragmentManager(), "tag");
			}
		
//		Intent i = getVideoIntent("/storage/sdcard0/SmartMob/video0"+val+".mp4");
//		c.startActivityForResult(i, 10);

	}
	
	public static Intent getVideoIntent(String fileUrl) {
	    Intent videoIntent = new Intent(Intent.ACTION_VIEW);
	    //videoIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    videoIntent.setDataAndType(Uri.fromFile(new File(fileUrl)), getMimeType(fileUrl));

	    return videoIntent;
	}

	public static boolean isEmpty(String string) {
	    return string == null || string.length() == 0;
	}

	public static String getMimeType(String url) {
	    String type = null;
	    String extension = MimeTypeMap.getFileExtensionFromUrl(url);

	    if (extension != null) {
	        MimeTypeMap mime = MimeTypeMap.getSingleton();
	        type = mime.getMimeTypeFromExtension(extension);

	        if (isEmpty(type))
	            type = "video/*"; // No MIME type found, so use the video wildcard
	    }

	    return type;
	}
}
